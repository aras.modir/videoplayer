package com.example.examplevideoplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.TextView;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;


public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private String url = "https://hw6.cdn.asset.aparat.com/aparat-video/443a63cca8c1921f53081b1fc769296c16444606-720p__50867.mp4";
    private Jzvd jzvdStd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        jzvdStd = findViewById(R.id.videoplayer);
        jzvdStd.setUp("https://hw6.cdn.asset.aparat.com/aparat-video/443a63cca8c1921f53081b1fc769296c16444606-720p__50867.mp4",
                "چک کردن پروفایل اینستاگرام ", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);

        textView = findViewById(R.id.text);
//        jzvdStd.setUp("https://hw6.cdn.asset.aparat.com/aparat-video/443a63cca8c1921f53081b1fc769296c16444606-720p__50867.mp4",
//                "چک کردن پروفایل اینستاگرام ");


    }

    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
//        Jzvd.FULLSCREEN_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;
//        Jzvd.NORMAL_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

    }

    @Override
    public void onBackPressed() {
        if (Jzvd.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Jzvd.FULLSCREEN_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
//        Jzvd.NORMAL_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }
}